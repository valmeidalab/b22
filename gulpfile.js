const gulp = require("gulp");
const ts = require("gulp-typescript");
const tsProject = ts.createProject("tsconfig.json");
const del = require('del');

gulp.task('clean', () => {
  return del(['dist/**/*']);
});  

gulp.task('copy', ['clean'], () => {
  return gulp.src('src/**/*.json')
    .pipe(gulp.dest('dist'));
});

gulp.task("default", ['copy'], () => {
  return tsProject.src()
      .pipe(tsProject())
      .js.pipe(gulp.dest("dist"));
});

gulp.watch( 'src/**/*', [ 'default' ] );
